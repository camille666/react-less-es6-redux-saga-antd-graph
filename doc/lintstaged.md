# lint-staged

## 一、工具选型

[预提交工具](https://www.npmtrends.com/lint-staged-vs-pre-commit-vs-pretty-quick)

1、lint-staged

检查提交一部分的文件，如果工程配套使用了 tslint 和 stylelint，推荐使用 lint-staged。

2、pretty-quick

检查修改的或者暂存的整个文件。

3、pre-commit

适用于多语言工程。

4、precise-commits

检查修改的或者暂存的部分文件，检查某段代码。

## 二、介绍 lint-staged

每次只对当前修改后的文件进行扫描，即对 git add 加入到 stage 区的文件进行扫描即可，完成对增量代码的检查。

1、git add 添加到暂存区；

2、执行 git commit；

3、husky 注册在 git pre-commit 的钩子函数被调用，执行 lint-staged；lint-staged 对所有被提交的文件依次执行写好的任务（tslint 和 stylelint）；如果有错误（没通过 TSlint 检查）则停止任务，同时打印错误信息，等待修复后再执行 commit；如果成功 commit，可 push 到远端仓库。
