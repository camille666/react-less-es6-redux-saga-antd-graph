# 需要注意一致性

## mock 数据

![mock数据](./mock-data-route.png)

## reducer 读取数据

![reducer读取数据](./reducer.png)

## 异步请求

![异步请求](./saga-request.png)
