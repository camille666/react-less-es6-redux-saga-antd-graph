import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router, Route, Switch, Redirect } from 'react-router-dom'

import { ConfigProvider } from 'antd'
// 由于 antd 组件的默认文案是英文，所以需要修改为中文
import zhCN from 'antd/es/locale/zh_CN'
// import moment from 'moment';
// import 'moment/locale/zh-cn';

import HomePage from 'modules/index'
import G6GraphView from 'modules/g6Graph/views/g6Graph'
import ToolBoxView from 'modules/g6Graph/views/toolBox'
import GraphEditorView from 'modules/g6Editor/g6Editor'

import 'styles/base.css'
import store from './store/store'
const createHistory = require('history').createBrowserHistory

class App extends React.Component {
    render() {
        return (
            <ConfigProvider locale={zhCN}>
                <Provider store={store}>
                    <Router history={createHistory()}>
                        <Route
                            render={() => {
                                return (
                                    <HomePage>
                                        <Switch>
                                            <Route
                                                exact
                                                path="/"
                                                render={() => (
                                                    <Redirect to="/g6Graph" />
                                                )}
                                            />
                                            <Route
                                                exact={true}
                                                path="/g6Graph"
                                                component={G6GraphView}
                                            />
                                            <Route
                                                exact={true}
                                                path="/toolBox"
                                                component={ToolBoxView}
                                            />
                                            <Route
                                                exact={true}
                                                path="/graphEditor"
                                                component={GraphEditorView}
                                            />
                                        </Switch>
                                    </HomePage>
                                )
                            }}
                        />
                    </Router>
                </Provider>
            </ConfigProvider>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'))
