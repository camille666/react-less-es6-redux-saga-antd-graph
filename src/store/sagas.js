import { fork, all } from 'redux-saga/effects'
import g6GraphSaga from 'modules/g6Graph/models/saga'

const sagas = [g6GraphSaga]

function* rootSaga() {
    yield all(
        sagas.map(fn => {
            return fn && fork(fn)
        })
    )
}
export default rootSaga
