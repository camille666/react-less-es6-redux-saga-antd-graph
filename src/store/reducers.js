import { combineReducers } from 'redux'
import reducerResult from 'modules/g6Graph/models/reducer'

const reducers = combineReducers({
    reducerResult
})

export default reducers
