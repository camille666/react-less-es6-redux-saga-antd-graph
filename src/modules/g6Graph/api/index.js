import axios from 'axios'

/**
 * 获取节点和关系列表
 */
export async function getNodesAndLinksArr(params) {
    return axios.get('/api/getNodesLinks', { data: params })
}

/**
 * 获取工具列表
 */
export async function getToolsBoxArr(params) {
    return axios.get('/api/getToolsBox', { data: params })
}
