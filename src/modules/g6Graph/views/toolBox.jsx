import React from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import * as Act from '../models/action'
import styles from './toolBox.less'

class ToolBox extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sourceData: [],
            dragData: []
        }
    }

    componentDidMount() {
        // 发请求，从接口获取节点数据
        const { dispatch } = this.props
        dispatch(Act.getToolsBoxArrAct())
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        // 拿到关系图的节点数据
        let sourceData = []
        let dragData = []
        if (!_.isEqual(nextProps.sourceData, prevState.sourceData)) {
            console.log(nextProps.sourceData)
            sourceData = nextProps.sourceData
            return { sourceData }
        }
        if (!_.isEqual(nextProps.dragData, prevState.dragData)) {
            console.log(nextProps.dragData)
            dragData = nextProps.dragData
            return { dragData }
        }
        return null
    }

    /*
     * 拖拽算子
     * 算子分为三类
     * source 只有出，没有入，kafka，redis
     * sink 只有入，没有出
     * trasformmation ，中间节点，有入有出
     */

    handleDrag(index, item) {
        this.props.dispatch(Act.setDragResultArrAct(item))
    }

    render() {
        const tools = this.state.sourceData
        const resultTools = this.state.dragData
        return (
            <div className={styles.toolBox}>
                <ul>
                    {tools &&
                        tools.length &&
                        tools.map((item, index) => {
                            return (
                                <li key={index}>
                                    <a
                                        draggable="true"
                                        onDragEnd={this.handleDrag.bind(
                                            this,
                                            index,
                                            item
                                        )}
                                    >
                                        {item.no}
                                    </a>
                                </li>
                            )
                        })}
                </ul>
                <div className={styles.receiverBox}>
                    {resultTools &&
                        resultTools.length &&
                        resultTools.map((item, index) => {
                            return (
                                <p className={styles.ball} key={index}>
                                    {item.instanceName}
                                </p>
                            )
                        })}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    if (state.reducerResult) {
        const sourceData = state.reducerResult.toolData.dragData
        // 这里必须取拖拽数据深拷贝，否则，react会理解dragData没变，不会重新去渲染界面
        const dragData = _.cloneDeep(state.reducerResult.dragResult.dragData)
        return {
            sourceData,
            dragData
        }
    }
}

export default connect(mapStateToProps)(ToolBox)
