import React from 'react'
import { connect } from 'react-redux'
import G6 from '@antv/g6'
import _ from 'lodash'
import * as Act from '../models/action'

class G6Graph extends React.Component {
    state = {
        gDataState: this.props.graphData
    }

    componentDidMount() {
        // 发请求，从接口获取节点数据
        const { dispatch } = this.props
        dispatch(Act.getNodesLinksArrAct())
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        // 拿到关系图的节点数据
        if (!_.isEqual(nextProps.graphData, prevState.gDataState)) {
            return {
                gDataState: nextProps.graphData
            }
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        this.drawG6RelationGraph()
    }

    // 初始化绘图
    drawG6RelationGraph() {
        const { gDataState } = this.state

        this.g6Graph = new G6.Graph({
            container: 'J_relation',
            width: window.innerWidth / 2,
            height: window.innerHeight / 2, // 画布高
            fitView: 'autoZoom'
        })

        if (this.g6Graph) {
            this.g6Graph.node({
                style: {
                    stroke: '#333'
                },
                label(model) {
                    return {
                        text: model.label,
                        fill: 'white'
                    }
                }
            })
            this.g6Graph.edge({
                style: {
                    endArrow: true
                }
            })
            this.g6Graph.on('afteritemdraw', ({ item }) => {
                const label = item.getLabel()
                if (label) {
                    label.set('freezePoint', {
                        x: 0,
                        y: 0
                    })
                }
            })
        }

        // 读入数据
        gDataState && this.g6Graph.read(gDataState)
        if (this.g6Graph) {
            /* 整体平移关系图
      this.g6Graph.on('drag', function(ev) {
        // 这里的this指向关系图
        let self = this;

        if (self.lastPoint) {
          self.translate(
            ev.domX - self.lastPoint.x,
            ev.domY - self.lastPoint.y
          );
        }
        self.lastPoint = {
          x: ev.domX,
          y: ev.domY
        };
      });
      this.g6Graph.on('dragend', function(ev) {
        // 这里的this指向关系图
        let self = this;
        
        self.lastPoint = undefined;
      });
      */

            // 拖拽节点开始
            this.g6Graph.on('node:dragstart', function(ev) {
                // 这里的this指向关系图
                const self = this
                const item = ev.item

                const model = item.getModel()
                self.node = item
                self.dx = model.x - ev.x
                self.dy = model.y - ev.y
            })

            // 拖拽节点
            this.g6Graph.on('node:drag', function(ev) {
                // 这里的this指向关系图
                const self = this
                self.node &&
                    self.update(self.node, {
                        x: ev.x + self.dx,
                        y: ev.y + self.dy
                    })
            })

            // 拖拽节点结束
            this.g6Graph.on('node:dragend', function(ev) {
                // 这里的this指向关系图
                const self = this

                self.node = undefined
            })

            // 添加节点
            this.g6Graph.add('node', {
                x: 1200,
                y: 600
            })
            // 添加关系
            this.g6Graph.add('edge', {
                source: 2,
                target: 0
            })
        }
    }

    render() {
        return (
            <div
                id="J_relation"
                ref={g6 => {
                    this.g6Graph = g6
                }}
            />
        )
    }
}

// 定义默认值
G6Graph.defaultProps = {}

const mapStateToProps = state => {
    if (state.reducerResult) {
        return {
            graphData: state.reducerResult.g6Graph.graphData
        }
    }
}

export default connect(mapStateToProps)(G6Graph)
