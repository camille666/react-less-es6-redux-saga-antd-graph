export const GET_NODES_LINKS_ARR = 'getNodesLinksArr'
export const SET_NODES_LINKS_ARR = 'setNodesLinksArr'
export const GET_TOOLS_BOX_ARR = 'getToolsBoxArr'
export const SET_TOOLS_BOX_ARR = 'setToolsBoxArr'
export const GET_DRAG_RESULT_ARR = 'getDragResultArr'
export const SET_DRAG_RESULT_ARR = 'setDragResultArr'

// 关系图节点（异步）
export function getNodesLinksArrAct() {
    return {
        type: GET_NODES_LINKS_ARR
    }
}
export function setNodesLinksArrAct(data) {
    return {
        type: SET_NODES_LINKS_ARR,
        data
    }
}

// 拖拽数据源
export function getToolsBoxArrAct() {
    return {
        type: GET_TOOLS_BOX_ARR
    }
}
export function setToolsBoxArrAct(data) {
    return {
        type: SET_TOOLS_BOX_ARR,
        data
    }
}

// 拖拽结果
export function getDragResultArrAct() {
    return {
        type: GET_DRAG_RESULT_ARR
    }
}
export function setDragResultArrAct(data) {
    return {
        type: SET_DRAG_RESULT_ARR,
        data
    }
}
