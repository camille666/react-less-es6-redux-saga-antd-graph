import { combineReducers } from 'redux'
import { cloneDeep, uniqBy } from 'lodash'
import * as Act from './action'

// 关系图
const initState = {
    graphData: {}
}

function g6Graph(state = initState, action) {
    let newState = cloneDeep(state)
    const result = action.data
    switch (action.type) {
        case Act.SET_NODES_LINKS_ARR:
            newState = Object.assign({}, newState, {
                graphData: result.data || {}
            })
            return newState
        default:
            return state
    }
}

// 拖拽数据源
const initDragState = {
    dragData: []
}

function toolData(state = initDragState, action) {
    let newState = cloneDeep(state)
    const result = action.data
    switch (action.type) {
        case Act.SET_TOOLS_BOX_ARR:
            newState = Object.assign({}, newState, {
                dragData: result.data.data || []
            })
            return newState
        default:
            return state
    }
}

/*
 * 拖拽数据存储可能有两种需求
 * 需求1、不去重，相同的元素可以拖N次
 * 需求2、去重，相同的元素只允许拖一遍
 */

// 拖拽结果
const initResultState = {
    dragData: []
}

const dragDataArr = []
let uniqueKeyArr = []
function dragResult(state = initResultState, action) {
    let newState = cloneDeep(state)
    const result = action.data
    switch (action.type) {
        case Act.SET_DRAG_RESULT_ARR:
            // 需求1、不去重，相同的元素可以拖N次
            dragDataArr.push(result)
            // 需求2、根据算子no去重，相同的元素只允许拖一遍
            uniqueKeyArr = uniqBy(dragDataArr, e => {
                return e.no
            })
            newState = Object.assign({}, newState, {
                dragData: uniqueKeyArr || []
            })
            return newState
        default:
            return state
    }
}

export default combineReducers({
    g6Graph,
    toolData,
    dragResult
})
