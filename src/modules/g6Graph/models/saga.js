import * as Act from './action'
import { takeEvery, call, put, fork, all } from 'redux-saga/effects'

import * as Fetch from '../api'

function* getNodesAndLinksArr() {
    const res = yield call(Fetch.getNodesAndLinksArr)
    yield put(Act.setNodesLinksArrAct(res))
}

function* _getNodesAndLinksArr() {
    yield takeEvery(Act.GET_NODES_LINKS_ARR, getNodesAndLinksArr)
}

function* getToolsBoxArr() {
    const res = yield call(Fetch.getToolsBoxArr)
    yield put(Act.setToolsBoxArrAct(res))
}

function* _getToolsBoxArr() {
    yield takeEvery(Act.GET_TOOLS_BOX_ARR, getToolsBoxArr)
}

export default function* root() {
    yield all([fork(_getNodesAndLinksArr), fork(_getToolsBoxArr)])
}
