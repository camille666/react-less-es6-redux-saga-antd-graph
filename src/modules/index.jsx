import React from 'react'
import { NavLink } from 'react-router-dom'
import { Layout, Menu, Icon } from 'antd'

const { Header, Sider, Content } = Layout

export default class HomePage extends React.Component {
    state = {
        collapsed: false
    }

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    render() {
        return (
            <Layout>
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}
                >
                    <div className="logo" />
                    <Menu
                        theme="dark"
                        mode="inline"
                        defaultSelectedKeys={['1']}
                    >
                        <Menu.Item key="1">
                            <Icon type="user" />
                            <span>
                                <NavLink to="/g6Graph">关系图</NavLink>
                            </span>
                        </Menu.Item>
                        <Menu.Item key="2">
                            <Icon type="google" />
                            <span>
                                <NavLink to="/toolBox">拖拽</NavLink>
                            </span>
                        </Menu.Item>
                        <Menu.Item key="3">
                            <Icon type="star" />
                            <span>
                                <NavLink to="/graphEditor">
                                    关系图编辑器
                                </NavLink>
                            </span>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <Icon
                            className="trigger"
                            type={
                                this.state.collapsed
                                    ? 'menu-unfold'
                                    : 'menu-fold'
                            }
                            onClick={this.toggle}
                        />
                    </Header>
                    <Content
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            background: '#fff',
                            minHeight: 280
                        }}
                    >
                        {this.props.children}
                    </Content>
                </Layout>
            </Layout>
        )
    }
}
