import React from 'react'
import G6Editor from '@antv/g6-editor'
import { Checkbox, Input } from 'antd'
import Editor from './snippets/editor'
import Navigator from './snippets/navigator'
import Contextmenu from './snippets/contextmenu'
import Page from './snippets/page'
import editorStyle from './snippets/editor.less'
import graphStyle from './g6Editor.less'

const Flow = G6Editor.Flow
// 注册算子基类
Flow.registerNode('sanko', {
    draw(item) {
        const group = item.getGraphicGroup()
        const model = item.getModel()
        const x = 0
        const y = 0
        const r = 20

        const keyShape = group.addShape('circle', {
            attrs: {
                x,
                y,
                r,
                fill: '#2dd885'
            }
        })

        // 名称文本
        const label = model.label ? model.label : this.label
        group.addShape('text', {
            attrs: {
                text: label,
                x: x + 12,
                y: y + 30,
                textAlign: 'center',
                textBaseline: 'middle',
                fill: '#2dd885'
            }
        })

        return keyShape
    },
    // 设置锚点
    anchor: [
        [0, 0.5], // 左边的中点
        [1, 0.5] // 右边的中点
    ]
})

// source算子
Flow.registerNode(
    'source-sanko',
    {
        label: 'source算子',
        // 设置锚点
        anchor: [
            [
                1,
                0.5,
                {
                    type: 'output'
                }
            ]
        ]
    },
    'sanko'
)

// sink算子
Flow.registerNode(
    'sink-sanko',
    {
        label: 'sink算子',
        // 设置锚点
        anchor: [
            [
                0,
                0.5,
                {
                    type: 'input'
                }
            ]
        ]
    },
    'sanko'
)

// transformation算子
Flow.registerNode(
    'transformation-sanko',
    {
        label: 'transformation算子',
        // 设置锚点
        anchor: [
            [
                0,
                0.5,
                {
                    type: 'input'
                }
            ],
            [
                1,
                0.5,
                {
                    type: 'output'
                }
            ]
        ]
    },
    'sanko'
)

export default class MyG6Editor extends Editor {
    componentDidMount() {
        setTimeout(() => {
            super.componentDidMount()
            const page = this.page

            // 输入锚点不可以连出边
            page.on('hoveranchor:beforeaddedge', ev => {
                if (ev.anchor.type === 'input') {
                    ev.cancel = true
                }
            })
            page.on('dragedge:beforeshowanchor', ev => {
                // 只允许目标锚点是输入，源锚点是输出，才能连接
                if (
                    !(
                        ev.targetAnchor.type === 'input' &&
                        ev.sourceAnchor.type === 'output'
                    )
                ) {
                    ev.cancel = true
                }
                // 如果拖动的是目标方向，则取消显示目标节点中已被连过的锚点
                if (
                    ev.dragEndPointType === 'target' &&
                    page.anchorHasBeenLinked(ev.target, ev.targetAnchor)
                ) {
                    ev.cancel = true
                }
                // 如果拖动的是源方向，则取消显示源节点中已被连过的锚点
                if (
                    ev.dragEndPointType === 'source' &&
                    page.anchorHasBeenLinked(ev.source, ev.sourceAnchor)
                ) {
                    ev.cancel = true
                }
            })

            /*
      //边点击事件
      let initGraph = page.getGraph();
      initGraph.on('edge:click', ev=>{
        console.log('点击边');
      });
      
      // 图数据变更后，保存关系图数据，读取关系图。
      page.on('afterchange', ev=>{
        let graphEditorData = page.save();
        let readGraphData = page.read(graphEditorData);
        console(readGraphData);
      }); */
        }, 100)
    }

    render() {
        const {
            curZoom,
            minZoom,
            maxZoom,
            inputingLabel,
            selectedModel
        } = this.state
        const labelInput = (
            <div className={graphStyle.p}>
                名称：
                <Input
                    size="small"
                    className="input name-input"
                    value={
                        inputingLabel !== null
                            ? inputingLabel
                            : selectedModel.label
                    }
                    onChange={ev => {
                        this.setState({
                            inputingLabel: ev.target.value
                        })
                    }}
                    onBlur={ev => {
                        this.updateGraph('label', ev.target.value)
                        this.setState({
                            inputingLabel: null
                        })
                    }}
                />
            </div>
        )
        return (
            <div id="editor" className={editorStyle.editor}>
                <div style={{ height: '42px' }}></div>
                <div className={graphStyle.bottomContainer}>
                    <Contextmenu />
                    <div id="itempannel" className={graphStyle.itempannel}>
                        <ul>
                            <li
                                className="getItem"
                                data-shape="source-sanko"
                                data-type="node"
                                data-size="42*42"
                            >
                                <span
                                    className={graphStyle.pannelTypeIcon}
                                ></span>
                                source算子
                            </li>
                            <li
                                className="getItem"
                                data-shape="sink-sanko"
                                data-type="node"
                                data-size="42*42"
                            >
                                <span
                                    className={graphStyle.pannelTypeIcon}
                                ></span>
                                sink算子
                            </li>
                            <li
                                className="getItem"
                                data-shape="transformation-sanko"
                                data-type="node"
                                data-size="42*42"
                            >
                                <span
                                    className={graphStyle.pannelTypeIcon}
                                ></span>
                                transformation算子
                            </li>
                        </ul>
                    </div>
                    <div id="detailpannel" className={graphStyle.detailpannel}>
                        <div
                            data-status="node-selected"
                            className="pannel"
                            id="node_detailpannel"
                        >
                            <div className={graphStyle.pannelTitle}>
                                算子详情
                            </div>
                            <div className={graphStyle.blockContainer}>
                                {labelInput}
                            </div>
                        </div>
                        <div
                            data-status="group-selected"
                            className="pannel"
                            id="node_detailpannel"
                        >
                            <div className={graphStyle.pannelTitle}>
                                群组详情
                            </div>
                            <div className={graphStyle.blockContainer}>
                                {labelInput}
                            </div>
                        </div>
                        <div
                            data-status="canvas-selected"
                            className="pannel"
                            id="canvas_detailpannel"
                        >
                            <div className={graphStyle.pannelTitle}>画布</div>
                            <div className={graphStyle.blockContainer}>
                                <Checkbox onChange={this.toggleGrid.bind(this)}>
                                    网格对齐
                                </Checkbox>
                            </div>
                        </div>
                    </div>
                    <Navigator
                        curZoom={curZoom}
                        minZoom={minZoom}
                        maxZoom={maxZoom}
                        changeZoom={this.changeZoom.bind(this)}
                    />
                    <Page />
                </div>
            </div>
        )
    }
}
