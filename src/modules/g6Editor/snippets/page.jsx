import React from 'react'
import styles from './page.less'

export default class Page extends React.Component {
    render() {
        return <div id="page" className={styles.page}></div>
    }
}
