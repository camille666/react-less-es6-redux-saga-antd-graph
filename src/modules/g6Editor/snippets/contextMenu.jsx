import React from 'react'
import contextMenuStyle from './contextmenu.less'

export default class Contextmenu extends React.Component {
    render() {
        return (
            <div
                id="contextmenu"
                className={'contextmenu ' + contextMenuStyle.contextmenu}
            >
                <div
                    data-status="node-selected"
                    className={'menu ' + contextMenuStyle.menu}
                >
                    <div
                        data-command="delete"
                        className={'command ' + contextMenuStyle.command}
                    >
                        <span>删除节点</span>
                        <span>delete</span>
                    </div>
                </div>
                <div
                    data-status="edge-selected"
                    className={'menu ' + contextMenuStyle.menu}
                >
                    <div
                        data-command="delete"
                        className={'command ' + contextMenuStyle.command}
                    >
                        <span>删除边</span>
                        <span>delete</span>
                    </div>
                </div>
            </div>
        )
    }
}
