# 可视化，编辑器

## 一、问题
1、版本问题

````
可以运行项目，正常
"@antv/g6": "^2.1.4-beta.3",
````

````
不能运行项目，异常
"@antv/g6": "^3.0.5-beta.7",
````

2、react版本
开始使用react的最新生命周期。

## 三、运行

1、安装依赖

````
yarn install
````

2、启动服务端

````
npm run server
````

3、启动客户端

````
npm run dev
````


babel-plugin-import实现antd的按需加载