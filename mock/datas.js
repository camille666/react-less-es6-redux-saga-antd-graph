const getNodesLinks = require('./data/getNodesLinks')
const getToolsBox = require('./data/getToolsBox')

module.exports = {
    getNodesLinks,
    getToolsBox
}
