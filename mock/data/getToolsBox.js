module.exports = {
    data: [
        {
            no: '1',
            opName: 'FieldOperator',
            instanceName: 'xxx'
        },
        {
            no: '2',
            opName: 'FieldCutOperator',
            instanceName: 'yyy'
        },
        {
            no: '3',
            opName: 'FieldOperator',
            instanceName: 'zzz'
        },
        {
            no: '4',
            opName: 'FieldCutOperator',
            instanceName: 'nnn'
        }
    ]
}
