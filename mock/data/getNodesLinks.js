module.exports = {
    nodes: [
        {
            x: 1169.8755903685553,
            y: 578.3192767110445,
            size: 30,
            color: '#70fcb8',
            id: 0,
            label: '甲',
            title: 'this is a box',
            type: 'Publication'
        },
        {
            x: 1133.9840064392847,
            y: 806.5813258362892,
            size: 40,
            color: '#2dd885',
            id: 1,
            label: '乙',
            title: 'this is  a computer',
            type: 'Publication'
        },
        {
            x: 1117.2866675861696,
            y: 632.3629043822336,
            size: 30,
            color: '#2dd885',
            id: 2,
            label: '丙',
            title: 'this is a book',
            type: 'Cross Domain'
        },
        {
            x: 1638.0345717584703,
            y: 810.9352305337666,
            size: 40,
            color: '#70fcb8',
            id: 3,
            label: '丁',
            title: 'this is ',
            type: 'Cross Domain'
        },
        {
            x: 1628.674560926354,
            y: 612.3154566077143,
            size: 40,
            color: '#2dd885',
            id: 4,
            label: '牛',
            title: ' i love france',
            type: 'Publication'
        }
    ],
    edges: [
        {
            source: 0,
            target: 4
        },
        {
            source: 1,
            target: 2
        },
        {
            source: 1,
            target: 3
        },
        {
            source: 2,
            target: 4
        },
        {
            source: 3,
            target: 4
        }
    ]
}
